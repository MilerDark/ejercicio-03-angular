import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {
  num1: number = 3;
  num2: number = 7;
  resultado: number;
  constructor() {
    this.resultado = this.num1 + this.num2;
   }
  ngOnInit(): void {
  }

}
